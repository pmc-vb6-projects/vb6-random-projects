VERSION 5.00
Begin VB.Form Form2 
   Caption         =   "Form2"
   ClientHeight    =   6765
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6720
   FillColor       =   &H00FF8080&
   ForeColor       =   &H00808080&
   LinkTopic       =   "Form2"
   ScaleHeight     =   6765
   ScaleWidth      =   6720
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Divide 
      Caption         =   "/"
      Height          =   495
      Left            =   3960
      TabIndex        =   13
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CommandButton Multiply 
      Caption         =   "*"
      Height          =   495
      Left            =   2880
      TabIndex        =   12
      Top             =   5400
      Width           =   735
   End
   Begin VB.CommandButton Minus 
      Caption         =   "-"
      Height          =   375
      Left            =   3960
      TabIndex        =   11
      Top             =   4800
      Width           =   1095
   End
   Begin VB.CommandButton Plus 
      Caption         =   "+"
      Height          =   375
      Left            =   2880
      TabIndex        =   10
      Top             =   4800
      Width           =   855
   End
   Begin VB.TextBox Num2 
      Height          =   615
      Left            =   1320
      TabIndex        =   9
      Text            =   "Text3"
      Top             =   5400
      Width           =   975
   End
   Begin VB.TextBox Num1 
      Height          =   495
      Left            =   1320
      TabIndex        =   8
      Text            =   "Text2"
      Top             =   4440
      Width           =   975
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Button"
      Height          =   375
      Left            =   1920
      TabIndex        =   7
      Top             =   3600
      Width           =   1575
   End
   Begin VB.TextBox Text1 
      Height          =   495
      Left            =   240
      TabIndex        =   6
      Text            =   "Text1"
      Top             =   3480
      Width           =   1455
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      ItemData        =   "Exercise 1.frx":0000
      Left            =   120
      List            =   "Exercise 1.frx":0002
      TabIndex        =   5
      Text            =   "Combo1"
      Top             =   1560
      Width           =   1455
   End
   Begin VB.OptionButton F 
      Caption         =   "F"
      Height          =   375
      Left            =   2760
      TabIndex        =   3
      Top             =   600
      Width           =   1095
   End
   Begin VB.OptionButton M 
      Caption         =   "M"
      Height          =   375
      Left            =   2760
      TabIndex        =   2
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      Caption         =   "Command1"
      DisabledPicture =   "Exercise 1.frx":0004
      Height          =   855
      Left            =   3000
      MaskColor       =   &H00C0C0FF&
      Picture         =   "Exercise 1.frx":1EE65
      TabIndex        =   1
      Top             =   2040
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   495
      Left            =   2760
      TabIndex        =   4
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Label L1 
      Caption         =   "Text1"
      Height          =   975
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   2055
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Test As Double


Private Sub Combo1_Click()
L1.Caption = Combo1.Text
MsgBox "This is Color " + Combo1.Text, vbOKCancel, "My Error"

End Sub

Private Sub Command2_Click()
Combo1.AddItem (Text1.Text)
End Sub

Private Sub F_DblClick()
Label2.Caption = "Female"
End Sub

Private Sub Form_Load()
Combo1.AddItem ("Red")
Combo1.AddItem ("Green")
Combo1.AddItem ("Blue")
Combo1.AddItem ("Yellow")
Combo1.AddItem ("Pink")


End Sub

Private Sub M_DblClick()
Label2.Caption = "Male"
End Sub

Private Sub Plus_Click()
Test = Val(Num1) + Val(Num2)
MsgBox "The answer is " & Test
End Sub
Private Sub Minus_Click()
MsgBox "The answer is " & Val(Num1) - Val(Num2)
End Sub
Private Sub Multiply_Click()
MsgBox "The answer is " & Val(Num1) * Val(Num2)
End Sub
Private Sub Divide_Click()
MsgBox "The answer is " & Val(Num1) / Val(Num2)
End Sub

