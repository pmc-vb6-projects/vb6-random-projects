VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00FFC0C0&
   Caption         =   "Ordering System"
   ClientHeight    =   7350
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7290
   LinkTopic       =   "Form1"
   Picture         =   "Form1.frx":0000
   ScaleHeight     =   7350
   ScaleWidth      =   7290
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Edit_Change 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5400
      TabIndex        =   31
      Top             =   6960
      Width           =   1455
   End
   Begin VB.TextBox Edit_Paid 
      Height          =   285
      Left            =   5400
      TabIndex        =   30
      Top             =   6600
      Width           =   1455
   End
   Begin VB.TextBox Edit_Total 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5400
      TabIndex        =   29
      Top             =   6240
      Width           =   1455
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   255
      Left            =   2520
      TabIndex        =   28
      Top             =   6600
      Width           =   1215
   End
   Begin VB.CommandButton cmdEnd 
      Caption         =   "End"
      Height          =   255
      Left            =   2520
      TabIndex        =   27
      Top             =   6960
      Width           =   1215
   End
   Begin VB.CommandButton cmdCompute 
      Caption         =   "Compute"
      Height          =   255
      Left            =   2520
      TabIndex        =   26
      Top             =   6240
      Width           =   1215
   End
   Begin VB.CheckBox chkVM5 
      Caption         =   "VM 5 (Coke, Palabok)...............................Php 50.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   22
      Top             =   5760
      Width           =   6615
   End
   Begin VB.CheckBox chkVM4 
      Caption         =   "VM 4 (Coke, Spaghetti).............................Php 50.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   21
      Top             =   5520
      Width           =   6615
   End
   Begin VB.CheckBox chkVM3 
      Caption         =   "VM 3 (Coke, Big Mac)...............................Php 75.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   20
      Top             =   5280
      Width           =   6615
   End
   Begin VB.CheckBox chkVM2 
      Caption         =   "VM 2 (Coke, Hamburger, French Fries)...............Php 65.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   19
      Top             =   5040
      Width           =   6615
   End
   Begin VB.CheckBox chkVM1 
      Caption         =   "VM 1 (Coke, Chicken, French Fries).................Php 65.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   240
      TabIndex        =   18
      Top             =   4800
      Width           =   6615
   End
   Begin VB.CheckBox chkPepsi 
      Caption         =   "Pepsi.............Php 15.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3720
      TabIndex        =   16
      Top             =   3960
      Width           =   3135
   End
   Begin VB.CheckBox chk4Season 
      Caption         =   "4 Season..........Php 30.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3720
      TabIndex        =   15
      Top             =   3720
      Width           =   3135
   End
   Begin VB.CheckBox chkSprite 
      Caption         =   "Sprite............Php 15.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3720
      TabIndex        =   14
      Top             =   3480
      Width           =   3135
   End
   Begin VB.CheckBox chkIceTea 
      Caption         =   "Ice Tea...........Php 20.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   240
      TabIndex        =   13
      Top             =   3720
      Width           =   3135
   End
   Begin VB.CheckBox chkCoke 
      Caption         =   "Coke..............Php 15.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   240
      TabIndex        =   12
      Top             =   3480
      Width           =   3135
   End
   Begin VB.CheckBox chkBigMAC 
      Caption         =   "Big-Mac...........Php 30.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3720
      TabIndex        =   11
      Top             =   2640
      Width           =   3135
   End
   Begin VB.CheckBox chkJollyHotdog 
      Caption         =   "Jolly Hotdog......Php 30.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3720
      TabIndex        =   10
      Top             =   2400
      Width           =   3135
   End
   Begin VB.CheckBox chkPalabok 
      Caption         =   "Palabok...........Php 30.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3720
      TabIndex        =   9
      Top             =   2160
      Width           =   3135
   End
   Begin VB.CheckBox chkSpaghetti 
      Caption         =   "Spaghetti.........Php 30.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3720
      TabIndex        =   8
      Top             =   1920
      Width           =   3135
   End
   Begin VB.CheckBox chkSundae 
      Caption         =   "Sundae............Php 20.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3720
      TabIndex        =   7
      Top             =   1680
      Width           =   3135
   End
   Begin VB.CheckBox chkFrenchFries 
      Caption         =   "French Fries......Php 30.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   6
      Top             =   2400
      Width           =   3135
   End
   Begin VB.CheckBox chkCheeseburger 
      Caption         =   "Cheeseburger......Php 30.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   5
      Top             =   2160
      Width           =   3135
   End
   Begin VB.CheckBox chkHamburger 
      Caption         =   "Hamburger.........Php 25.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   4
      Top             =   1920
      Width           =   3135
   End
   Begin VB.CheckBox chkChicken 
      Caption         =   "Chicken...........Php 35.00"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   240
      TabIndex        =   3
      Top             =   1680
      Width           =   3135
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "CHANGE:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3840
      TabIndex        =   25
      Top             =   6960
      Width           =   1455
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "AMOUNT PAID:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3840
      TabIndex        =   24
      Top             =   6600
      Width           =   1455
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "TOTAL:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3840
      TabIndex        =   23
      Top             =   6240
      Width           =   1455
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "VALUE MEALS:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   17
      Top             =   4320
      Width           =   6615
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "BEVERAGES:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   3000
      Width           =   6615
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "EMPLOYEE NAME:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   1200
      Width           =   6615
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "ORDERING SYSTEM"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   6855
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ValueMeal As Integer


Private Sub chk4Season_Click()

If chk4Season = 1 Then Edit_Total = Val(Edit_Total) + 30
If chk4Season = 0 Then Edit_Total = Val(Edit_Total) - 30

End Sub

Private Sub chkBigMAC_Click()

If chkBigMAC = 1 Then Edit_Total = Val(Edit_Total) + 30
If chkBigMAC = 0 Then Edit_Total = Val(Edit_Total) - 30

End Sub

Private Sub chkCheeseburger_Click()

If chkCheeseburger = 1 Then Edit_Total = Val(Edit_Total) + 30
If chkCheeseburger = 0 Then Edit_Total = Val(Edit_Total) - 30

End Sub

Private Sub chkChicken_Click()

If chkChicken = 1 Then Edit_Total = Val(Edit_Total) + 35
If chkChicken = 0 Then Edit_Total = Val(Edit_Total) - 35

End Sub

Private Sub chkCoke_Click()

If chkCoke = 1 Then Edit_Total = Val(Edit_Total) + 15
If chkCoke = 0 Then Edit_Total = Val(Edit_Total) - 15

End Sub

Private Sub chkFrenchFries_Click()

If chkFrenchFries = 1 Then Edit_Total = Val(Edit_Total) + 30
If chkFrenchFries = 0 Then Edit_Total = Val(Edit_Total) - 30

End Sub

Private Sub chkHamburger_Click()

If chkHamburger = 1 Then Edit_Total = Val(Edit_Total) + 25
If chkHamburger = 0 Then Edit_Total = Val(Edit_Total) - 25

End Sub

Private Sub chkIceTea_Click()

If chkIceTea = 1 Then Edit_Total = Val(Edit_Total) + 20
If chkIceTea = 0 Then Edit_Total = Val(Edit_Total) - 20

End Sub

Private Sub chkJollyHotdog_Click()

If chkJollyHotdog = 1 Then Edit_Total = Val(Edit_Total) + 20
If chkJollyHotdog = 0 Then Edit_Total = Val(Edit_Total) - 20

End Sub

Private Sub chkPalabok_Click()

If chkPalabok = 1 Then Edit_Total = Val(Edit_Total) + 30
If chkPalabok = 0 Then Edit_Total = Val(Edit_Total) - 30

End Sub

Private Sub chkPepsi_Click()

If chkPepsi = 1 Then Edit_Total = Val(Edit_Total) + 15
If chkPepsi = 0 Then Edit_Total = Val(Edit_Total) - 15

End Sub

Private Sub chkSpaghetti_Click()

If chkSpaghetti = 1 Then Edit_Total = Val(Edit_Total) + 30
If chkSpaghetti = 0 Then Edit_Total = Val(Edit_Total) - 30

End Sub

Private Sub chkSprite_Click()

If chkSprite = 1 Then Edit_Total = Val(Edit_Total) + 15
If chkSprite = 0 Then Edit_Total = Val(Edit_Total) - 15

End Sub

Private Sub chkSundae_Click()

If chkSundae = 1 Then Edit_Total = Val(Edit_Total) + 20
If chkSundae = 0 Then Edit_Total = Val(Edit_Total) - 20

End Sub

Private Sub chkVM1_Click()

If chkVM1 = 1 Then

    If chkCoke = 0 Then
        chkCoke = 1
        Edit_Total = Edit_Total - 15
    End If
    
    If chkHamburger = 0 Then
        chkHamburger = 1
        Edit_Total = Edit_Total - 25
    End If
    
    If chkChicken = 0 Then
        chkChicken = 1
        Edit_Total = Edit_Total - 35
    End If
        
    Edit_Total = Edit_Total + 65
    

End If

If chkVM1 = 0 Then

    If chkCoke = 1 Then
        chkCoke = 0
        Edit_Total = Edit_Total + 15
    End If
    
    If chkHamburger = 1 Then
        chkHamburger = 0
        Edit_Total = Edit_Total + 25
    End If
    
    If chkChicken = 1 Then
        chkChicken = 0
        Edit_Total = Edit_Total + 35
    End If
        
    Edit_Total = Edit_Total - 65
    
End If
End Sub

Private Sub chkVM2_Click()

If chkVM2 = 1 Then

    If chkCoke = 0 Then
        chkCoke = 1
        Edit_Total = Edit_Total - 15
    End If
    
    If chkHamburger = 0 Then
        chkHamburger = 1
        Edit_Total = Edit_Total - 25
    End If
    
    If chkFrenchFries = 0 Then
        chkFrenchFries = 1
        Edit_Total = Edit_Total - 30
    End If
        
    Edit_Total = Edit_Total + 65
    
End If

If chkVM2 = 0 Then

    If chkCoke = 1 Then
        chkCoke = 0
        Edit_Total = Edit_Total + 15
    End If
    
    If chkHamburger = 1 Then
        chkHamburger = 0
        Edit_Total = Edit_Total + 25
    End If
    
    If chkFrenchFries = 1 Then
        chkFrenchFries = 0
        Edit_Total = Edit_Total + 30
    End If
        
    Edit_Total = Edit_Total - 65

End If
End Sub

Private Sub chkVM3_Click()

If chkVM3 = 1 Then

    If chkCoke = 0 Then
        chkCoke = 1
        Edit_Total = Edit_Total - 15
    End If
    
    If chkBigMAC = 0 Then
        chkBigMAC = 1
        Edit_Total = Edit_Total - 30
    End If
        
    Edit_Total = Edit_Total + 75
     
End If

If chkVM3 = 0 Then

    If chkCoke = 1 Then
        chkCoke = 0
        Edit_Total = Edit_Total + 15
    End If
    
    If chkBigMAC = 1 Then
        chkBigMAC = 0
        Edit_Total = Edit_Total + 30
    End If
        
    Edit_Total = Edit_Total - 75
     
    
End If
End Sub

Private Sub chkVM4_Click()

If chkVM4 = 1 Then

    If chkCoke = 0 Then
        chkCoke = 1
        Edit_Total = Edit_Total - 15
    End If
    
    If chkSpaghetti = 0 Then
        chkSpaghetti = 1
        Edit_Total = Edit_Total - 30
    End If
        
    Edit_Total = Edit_Total + 50

End If

If chkVM4 = 0 Then

    If chkCoke = 1 Then
        chkCoke = 0
        Edit_Total = Edit_Total + 15
    End If
    
    If chkSpaghetti = 1 Then
        chkSpaghetti = 0
        Edit_Total = Edit_Total + 30
    End If
    
    Edit_Total = Edit_Total - 50

End If
End Sub

Private Sub chkVM5_Click()


If chkVM5 = 1 Then

    If chkCoke = 0 Then
        chkCoke = 1
        Edit_Total = Edit_Total - 15
    End If
    
    If chkPalabok = 0 Then
        chkPalabok = 1
        Edit_Total = Edit_Total - 30
    End If
        
    Edit_Total = Edit_Total + 50
       
End If

If chkVM5 = 0 Then

    If chkCoke = 1 Then
        chkCoke = 0
        Edit_Total = Edit_Total + 15
    End If
    
    If chkPalabok = 1 Then
        chkPalabok = 0
        Edit_Total = Edit_Total + 30
    End If
    
    Edit_Total = Edit_Total - 50
   
End If

End Sub

Private Sub cmdClear_Click()

chk4Season = 0
chkBigMAC = 0
chkCheeseburger = 0
chkChicken = 0
chkCoke = 0
chkFrenchFries = 0
chkHamburger = 0
chkIceTea = 0
chkJollyHotdog = 0
chkPalabok = 0
chkPepsi = 0
chkSpaghetti = 0
chkSprite = 0
chkSundae = 0

chkVM1 = 0
chkVM2 = 0
chkVM3 = 0
chkVM4 = 0
chkVM5 = 0

Edit_Total = 0
Edit_Paid = 0
Edit_Change = 0

End Sub

Private Sub cmdCompute_Click()

Edit_Change = Val(Edit_Paid) - Val(Edit_Total)

End Sub

Private Sub cmdEnd_Click()

End

End Sub

