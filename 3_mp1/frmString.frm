VERSION 5.00
Begin VB.Form frmString 
   BackColor       =   &H00FFC0C0&
   Caption         =   "String Manipulation"
   ClientHeight    =   4590
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4875
   LinkTopic       =   "Form1"
   ScaleHeight     =   3422.819
   ScaleMode       =   0  'User
   ScaleWidth      =   4875
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdNext 
      Caption         =   "NEXT"
      Height          =   375
      Left            =   3600
      TabIndex        =   11
      Top             =   4080
      Width           =   1095
   End
   Begin VB.CommandButton cmdRtrim 
      Caption         =   "Rtrim"
      Height          =   670
      Left            =   120
      TabIndex        =   10
      Top             =   960
      Width           =   1500
   End
   Begin VB.CommandButton cmdRight 
      Caption         =   "Right"
      Height          =   670
      Left            =   120
      TabIndex        =   9
      Top             =   1680
      Width           =   1500
   End
   Begin VB.CommandButton cmdAlltrim 
      Caption         =   "Trim"
      Height          =   670
      Left            =   120
      TabIndex        =   8
      Top             =   2400
      Width           =   1500
   End
   Begin VB.CommandButton cmdUcase 
      Caption         =   "Ucase"
      Height          =   670
      Left            =   1680
      TabIndex        =   7
      Top             =   2400
      Width           =   1500
   End
   Begin VB.CommandButton cmdLcase 
      Caption         =   "Lcase"
      Height          =   670
      Left            =   3240
      TabIndex        =   6
      Top             =   2400
      Width           =   1500
   End
   Begin VB.CommandButton cmdMid 
      Caption         =   "Mid"
      Height          =   670
      Left            =   3240
      TabIndex        =   5
      Top             =   1680
      Width           =   1500
   End
   Begin VB.CommandButton cmdLen 
      Caption         =   "Len"
      Height          =   670
      Left            =   1680
      TabIndex        =   4
      Top             =   1680
      Width           =   1500
   End
   Begin VB.CommandButton cmdLeft 
      Caption         =   "Left"
      Height          =   670
      Left            =   3240
      TabIndex        =   3
      Top             =   960
      Width           =   1500
   End
   Begin VB.CommandButton cmdLtrim 
      Caption         =   "Ltrim"
      Height          =   670
      Left            =   1680
      TabIndex        =   2
      Top             =   960
      Width           =   1500
   End
   Begin VB.TextBox edit 
      Alignment       =   2  'Center
      Height          =   670
      Left            =   120
      TabIndex        =   1
      Top             =   3240
      Width           =   4575
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "String Manipulation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   4575
   End
End
Attribute VB_Name = "frmString"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim temp As Integer
Dim temp2 As Integer



Private Sub cmdAlltrim_Click()
edit.Text = Trim(edit.Text)
End Sub

Private Sub cmdLcase_Click()
edit.Text = LCase(edit.Text)
End Sub

Private Sub cmdLeft_Click()
temp = InputBox("Please Enter A Number:", "Question")
edit.Text = Left(edit.Text, temp)
End Sub

Private Sub cmdLen_Click()
edit.Text = Len(edit.Text)
End Sub

Private Sub cmdLtrim_Click()
edit.Text = LTrim(edit.Text)
End Sub

Private Sub cmdMid_Click()
temp = InputBox("Please Enter The FIRST number:", "Question")
temp2 = InputBox("Please Enter The SECOND number:", "Question")
edit.Text = Mid(edit.Text, temp, temp2)
End Sub

Private Sub cmdNext_Click()
frmString.Hide
frmString2.Show

End Sub

Private Sub cmdRight_Click()
temp = InputBox("Please Enter A Number:", "Question")
edit.Text = Right(edit.Text, temp)
End Sub

Private Sub cmdRtrim_Click()
edit.Text = RTrim(edit.Text)
End Sub

Private Sub cmdUcase_Click()
edit.Text = UCase(edit.Text)
End Sub

