VERSION 5.00
Begin VB.Form frmString2 
   BackColor       =   &H00FFC0FF&
   Caption         =   "String Manipulation 2"
   ClientHeight    =   4590
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4875
   LinkTopic       =   "Form1"
   ScaleHeight     =   4590
   ScaleWidth      =   4875
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdBack 
      Caption         =   "BACK"
      Height          =   375
      Left            =   3600
      TabIndex        =   6
      Top             =   4080
      Width           =   1095
   End
   Begin VB.CommandButton cmdCount 
      Caption         =   "Count"
      Height          =   495
      Left            =   1800
      TabIndex        =   5
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox edit3 
      Alignment       =   2  'Center
      Height          =   500
      Left            =   120
      TabIndex        =   4
      Top             =   2520
      Width           =   4575
   End
   Begin VB.TextBox edit2 
      Alignment       =   2  'Center
      Height          =   500
      Left            =   120
      TabIndex        =   2
      Top             =   1320
      Width           =   4575
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Enter a Character:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   2040
      Width           =   4575
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Enter A Word:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   4575
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "String Manipulation 2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   4455
   End
End
Attribute VB_Name = "frmString2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim counter As Integer
Dim number As Integer

Private Sub cmdBack_Click()
frmString2.Hide
frmString.Show

End Sub

Private Sub cmdCount_Click()
number = 0

For counter = 1 To Len(edit2.Text)
If Mid(edit2.Text, counter, 1) = edit3.Text Then number = number + 1
Next

MsgBox "There are " & number & " " & edit3.Text, vbInformation + vbOKOnly, "Answer"
End Sub

