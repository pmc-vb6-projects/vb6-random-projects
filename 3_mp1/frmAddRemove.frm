VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmAddRemove 
   BackColor       =   &H80000008&
   Caption         =   "Add / Remove"
   ClientHeight    =   6675
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10875
   LinkTopic       =   "Form1"
   ScaleHeight     =   6675
   ScaleWidth      =   10875
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdA 
      Caption         =   "A"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6600
      TabIndex        =   17
      Top             =   4200
      Width           =   255
   End
   Begin VB.CommandButton cmdC 
      Caption         =   "C"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8640
      TabIndex        =   16
      Top             =   4200
      Width           =   255
   End
   Begin VB.CommandButton cmdB 
      Caption         =   "B"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6600
      TabIndex        =   15
      Top             =   4800
      Width           =   255
   End
   Begin VB.CommandButton cmdD 
      Caption         =   "D"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8640
      TabIndex        =   14
      Top             =   4800
      Width           =   255
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8160
      TabIndex        =   13
      Top             =   6000
      Width           =   735
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7320
      TabIndex        =   12
      Top             =   6000
      Width           =   735
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6480
      TabIndex        =   11
      Top             =   6000
      Width           =   735
   End
   Begin VB.TextBox txtQuestion 
      BackColor       =   &H00800000&
      DataField       =   "Question"
      DataSource      =   "adoQuest"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   1695
      Left            =   6600
      TabIndex        =   7
      Text            =   "Text1"
      Top             =   2040
      Width           =   3975
   End
   Begin VB.TextBox txtAnswerA 
      BackColor       =   &H00800000&
      DataField       =   "AnswerA"
      DataSource      =   "adoQuest"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   6960
      TabIndex        =   6
      Text            =   "Text2"
      Top             =   4200
      Width           =   1455
   End
   Begin VB.TextBox txtAnswerB 
      BackColor       =   &H00800000&
      DataField       =   "AnswerB"
      DataSource      =   "adoQuest"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   6960
      TabIndex        =   5
      Text            =   "Text2"
      Top             =   4800
      Width           =   1455
   End
   Begin VB.TextBox txtAnswerC 
      BackColor       =   &H00800000&
      DataField       =   "AnswerC"
      DataSource      =   "adoQuest"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   9120
      TabIndex        =   4
      Text            =   "Text2"
      Top             =   4200
      Width           =   1455
   End
   Begin VB.TextBox txtAnswerD 
      BackColor       =   &H00800000&
      DataField       =   "AnswerD"
      DataSource      =   "adoQuest"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   9120
      TabIndex        =   3
      Text            =   "Text2"
      Top             =   4800
      Width           =   1455
   End
   Begin VB.TextBox txtCorrect 
      BackColor       =   &H00800000&
      DataField       =   "Correct"
      DataSource      =   "adoQuest"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   9120
      TabIndex        =   2
      Text            =   "Text2"
      Top             =   5400
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Main Menu"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9120
      TabIndex        =   1
      Top             =   6000
      Width           =   1455
   End
   Begin MSAdodcLib.Adodc adoQuest 
      Height          =   375
      Left            =   6480
      Top             =   1080
      Width           =   4035
      _ExtentX        =   7117
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   0
      BackColor       =   8388608
      ForeColor       =   -2147483643
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Questionaires.mdb;Persist Security Info=False"
      OLEDBString     =   "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Questionaires.mdb;Persist Security Info=False"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "Questionaiers"
      Caption         =   "Questionaiers"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Answer:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   6480
      TabIndex        =   10
      Top             =   3840
      Width           =   2055
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Question:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   6360
      TabIndex        =   9
      Top             =   1560
      Width           =   2055
   End
   Begin VB.Label lblAnswer 
      BackStyle       =   0  'Transparent
      Caption         =   "THE ANSWER IS:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   6960
      TabIndex        =   8
      Top             =   5400
      Width           =   2055
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00800000&
      Caption         =   "ADD / REMOVE"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   495
      Left            =   4560
      TabIndex        =   0
      Top             =   360
      Width           =   4575
   End
   Begin VB.Image Image1 
      Height          =   6570
      Left            =   -720
      Picture         =   "frmAddRemove.frx":0000
      Top             =   120
      Width           =   7035
   End
End
Attribute VB_Name = "frmAddRemove"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAdd_Click()
adoQuest.Recordset.AddNew
End Sub

Private Sub cmdDelete_Click()
adoQuest.Recordset.Delete

txtQuestion.Text = ""
txtAnswerA.Text = ""
txtAnswerB.Text = ""
txtAnswerC.Text = ""
txtAnswerD.Text = ""
txtCorrect.Text = ""

End Sub

Private Sub cmdSave_Click()
adoQuest.Recordset.Fields("Question") = txtQuestion.Text
adoQuest.Recordset.Fields("AnswerA") = txtAnswerA.Text
adoQuest.Recordset.Fields("AnswerB") = txtAnswerB.Text
adoQuest.Recordset.Fields("AnswerC") = txtAnswerC.Text
adoQuest.Recordset.Fields("AnswerD") = txtAnswerD.Text
adoQuest.Recordset.Fields("Correct") = txtCorrect.Text
adoQuest.Recordset.Update

End Sub

Private Sub Command1_Click()
frmAddRemove.Hide
frmInterface.Show
End Sub

