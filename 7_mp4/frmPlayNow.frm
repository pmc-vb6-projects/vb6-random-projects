VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmPlayNow 
   BackColor       =   &H80000007&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Play Now!"
   ClientHeight    =   6555
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10875
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6555
   ScaleWidth      =   10875
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtCorrect 
      BackColor       =   &H00800000&
      DataField       =   "Correct"
      DataSource      =   "adoQuest"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   6960
      TabIndex        =   18
      Top             =   5400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "RESET"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9120
      TabIndex        =   17
      Top             =   5400
      Width           =   1455
   End
   Begin VB.TextBox txtQuestNum 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   495
      Left            =   9960
      TabIndex        =   16
      Text            =   "10"
      Top             =   360
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txtScore 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800000&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   7560
      TabIndex        =   13
      Top             =   6000
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Main Menu"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9120
      TabIndex        =   12
      Top             =   6000
      Width           =   1455
   End
   Begin MSAdodcLib.Adodc adoQuest 
      Height          =   375
      Left            =   6480
      Top             =   1080
      Visible         =   0   'False
      Width           =   4155
      _ExtentX        =   7329
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   0
      BackColor       =   8388608
      ForeColor       =   -2147483643
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Questionaires.mdb;Persist Security Info=False"
      OLEDBString     =   "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Questionaires.mdb;Persist Security Info=False"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "Questionaiers"
      Caption         =   "Questionaiers"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtAnswerD 
      BackColor       =   &H00800000&
      DataField       =   "AnswerD"
      DataSource      =   "adoQuest"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   9120
      TabIndex        =   11
      Top             =   4800
      Width           =   1455
   End
   Begin VB.TextBox txtAnswerC 
      BackColor       =   &H00800000&
      DataField       =   "AnswerC"
      DataSource      =   "adoQuest"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   9120
      TabIndex        =   10
      Top             =   4200
      Width           =   1455
   End
   Begin VB.TextBox txtAnswerB 
      BackColor       =   &H00800000&
      DataField       =   "AnswerB"
      DataSource      =   "adoQuest"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   6960
      TabIndex        =   9
      Top             =   4800
      Width           =   1455
   End
   Begin VB.TextBox txtAnswerA 
      BackColor       =   &H00800000&
      DataField       =   "AnswerA"
      DataSource      =   "adoQuest"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   375
      Left            =   6960
      TabIndex        =   8
      Top             =   4200
      Width           =   1455
   End
   Begin VB.CommandButton cmdD 
      Caption         =   "D"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8640
      TabIndex        =   5
      Top             =   4800
      Width           =   255
   End
   Begin VB.CommandButton cmdB 
      Caption         =   "B"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6600
      TabIndex        =   4
      Top             =   4800
      Width           =   255
   End
   Begin VB.CommandButton cmdC 
      Caption         =   "C"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8640
      TabIndex        =   3
      Top             =   4200
      Width           =   255
   End
   Begin VB.CommandButton cmdA 
      Caption         =   "A"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6600
      TabIndex        =   2
      Top             =   4200
      Width           =   255
   End
   Begin VB.TextBox txtQuestion 
      BackColor       =   &H00800000&
      DataField       =   "Question"
      DataSource      =   "adoQuest"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   1695
      Left            =   6600
      MultiLine       =   -1  'True
      TabIndex        =   1
      Text            =   "frmPlayNow.frx":0000
      Top             =   2040
      Width           =   3975
   End
   Begin VB.Label lblTurn 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7680
      TabIndex        =   15
      Top             =   1560
      Width           =   2895
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "MONEY:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   6480
      TabIndex        =   14
      Top             =   6000
      Width           =   975
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Answer:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   6480
      TabIndex        =   7
      Top             =   3840
      Width           =   2055
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Question:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   6360
      TabIndex        =   6
      Top             =   1560
      Width           =   2055
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00800000&
      Caption         =   "PLAY NOW!"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   495
      Left            =   4560
      TabIndex        =   0
      Top             =   360
      Width           =   4575
   End
   Begin VB.Image Image1 
      Height          =   6570
      Left            =   -720
      Picture         =   "frmPlayNow.frx":0006
      Top             =   0
      Width           =   7035
   End
End
Attribute VB_Name = "frmPlayNow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Players As Integer
Dim Player(32) As String
Dim Player_Score(32) As Integer
Dim Focus As Integer
Dim Temp As Integer
Dim Temp2 As Integer

Dim ctr As Integer
Dim ctr_string As String


Private Sub cmdA_Click()
   
If txtAnswerA.Text = txtCorrect.Text Then
    txtScore.Text = Val(txtScore.Text) + 2 ^ Val(txtQuestNum.Text)
    Player_Score(Focus) = Val(txtScore.Text)
    MsgBox "Correct! You Won " + txtScore.Text + " Pesos!", , "Reply"
    
    adoQuest.Recordset.MoveNext
    
    If txtQuestion.Text = "" Then
        For ctr = 1 To Players
            If Player_Score(ctr) > Temp Then
                Temp2 = ctr
                Temp = Player_Score(ctr)
            End If
        Next
        
        lblTurn.Caption = ""
        txtScore.Text = ""
        MsgBox Player(Temp2) + " is the WINNER!", , "Reply"
        frmInterface.Show
        frmPlayNow.Hide
    End If
    
    Focus = Focus + 1
    If Focus > Players Then Focus = 1
    lblTurn.Caption = "It's " + Player(Focus) + "'s Turn!"
    txtScore.Text = Player_Score(Focus)
            
Else
    MsgBox "Wrong. The answer is: " + txtCorrect.Text, , "Reply"
    adoQuest.Recordset.MoveNext
    
    Focus = Focus + 1
    If Focus > Players Then Focus = 1
    lblTurn.Caption = "It's " + Player(Focus) + "'s Turn!"
    txtScore.Text = Player_Score(Focus)
    
End If

txtQuestNum.Text = Val(txtQuestNum.Text) + 1

End Sub

Private Sub cmdB_Click()
If txtAnswerB.Text = txtCorrect.Text Then
    txtScore.Text = Val(txtScore.Text) + 2 ^ Val(txtQuestNum.Text)
    Player_Score(Focus) = Val(txtScore.Text)
    MsgBox "Correct! You Won " + txtScore.Text + " Pesos!", , "Reply"
    
    adoQuest.Recordset.MoveNext
    
    If txtQuestion.Text = "" Then
        For ctr = 1 To Players
            If Player_Score(ctr) > Temp Then
                Temp2 = ctr
                Temp = Player_Score(ctr)
            End If
        Next
        
        lblTurn.Caption = ""
        txtScore.Text = ""
        MsgBox Player(Temp2) + " is the WINNER!", , "Reply"
        frmInterface.Show
        frmPlayNow.Hide
    End If
    
    Focus = Focus + 1
    If Focus > Players Then Focus = 1
    lblTurn.Caption = "It's " + Player(Focus) + "'s Turn!"
    txtScore.Text = Player_Score(Focus)

Else
    MsgBox "Wrong. The answer is: " + txtCorrect.Text, , "Reply"
    adoQuest.Recordset.MoveNext
    
    Focus = Focus + 1
    If Focus > Players Then Focus = 1
    lblTurn.Caption = "It's " + Player(Focus) + "'s Turn!"
    txtScore.Text = Player_Score(Focus)
    
End If

txtQuestNum.Text = Val(txtQuestNum.Text) + 1

End Sub

Private Sub cmdC_Click()
If txtAnswerC.Text = txtCorrect.Text Then
    txtScore.Text = Val(txtScore.Text) + 2 ^ Val(txtQuestNum.Text)
    Player_Score(Focus) = Val(txtScore.Text)
    MsgBox "Correct! You Won " + txtScore.Text + " Pesos!", , "Reply"
    
    adoQuest.Recordset.MoveNext
    
    If txtQuestion.Text = "" Then
        For ctr = 1 To Players
            If Player_Score(ctr) > Temp Then
                Temp2 = ctr
                Temp = Player_Score(ctr)
            End If
        Next
        
        lblTurn.Caption = ""
        txtScore.Text = ""
        MsgBox Player(Temp2) + " is the WINNER!", , "Reply"
        frmInterface.Show
        frmPlayNow.Hide
    End If
    
    Focus = Focus + 1
    If Focus > Players Then Focus = 1
    lblTurn.Caption = "It's " + Player(Focus) + "'s Turn!"
    txtScore.Text = Player_Score(Focus)
Else
    MsgBox "Wrong. The answer is: " + txtCorrect.Text, , "Reply"
    adoQuest.Recordset.MoveNext
    
    Focus = Focus + 1
    If Focus > Players Then Focus = 1
    lblTurn.Caption = "It's " + Player(Focus) + "'s Turn!"
    txtScore.Text = Player_Score(Focus)
    
End If

txtQuestNum.Text = Val(txtQuestNum.Text) + 1

End Sub

Private Sub cmdD_Click()
If txtAnswerD.Text = txtCorrect.Text Then
    txtScore.Text = Val(txtScore.Text) + 2 ^ Val(txtQuestNum.Text)
    Player_Score(Focus) = Val(txtScore.Text)
    MsgBox "Correct! You Won " + txtScore.Text + " Pesos!", , "Reply"
    
    adoQuest.Recordset.MoveNext
    
    If txtQuestion.Text = "" Then
        For ctr = 1 To Players
            If Player_Score(ctr) > Temp Then
                Temp2 = ctr
                Temp = Player_Score(ctr)
            End If
        Next
        
        lblTurn.Caption = ""
        txtScore.Text = ""
        MsgBox Player(Temp2) + " is the WINNER!", , "Reply"
        frmInterface.Show
        frmPlayNow.Hide
    End If
    
    Focus = Focus + 1
    If Focus > Players Then Focus = 1
    lblTurn.Caption = "It's " + Player(Focus) + "'s Turn!"
    txtScore.Text = Player_Score(Focus)

Else
    MsgBox "Wrong. The answer is: " + txtCorrect.Text, , "Reply"
    adoQuest.Recordset.MoveNext
    
    Focus = Focus + 1
    If Focus > Players Then Focus = 1
    lblTurn.Caption = "It's " + Player(Focus) + "'s Turn!"
    txtScore.Text = Player_Score(Focus)
    
End If

txtQuestNum.Text = Val(txtQuestNum.Text) + 1

End Sub

Private Sub cmdReset_Click()
Focus = 1
Temp = 0
Temp2 = 0
txtQuestNum = 10
txtScore = 0

adoQuest.Recordset.MoveFirst

Players = InputBox("How Many Players", "Players")

For ctr = 1 To Players
ctr_string = ctr
Player(ctr) = InputBox("Enter Name for Player " + ctr_string, "Player Name")
Player_Score(ctr) = 0
Next

lblTurn.Caption = "It's " + Player(1) + "'s Turn!"
End Sub

Private Sub Command1_Click()
frmInterface.Show
frmPlayNow.Hide
End Sub

Private Sub Form_Load()
Focus = 1
Temp = 0
Temp2 = 0
txtQuestNum = 10
txtScore = 0

adoQuest.Recordset.MoveFirst

Players = InputBox("How Many Players", "Players")

For ctr = 1 To Players
ctr_string = ctr
Player(ctr) = InputBox("Enter Name for Player " + ctr_string, "Player Name")
Player_Score(ctr) = 0
Next

lblTurn.Caption = "It's " + Player(1) + "'s Turn!"
End Sub

