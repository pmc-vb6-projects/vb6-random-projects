VERSION 5.00
Begin VB.Form frmInterface 
   BackColor       =   &H00000000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Main Menu"
   ClientHeight    =   6570
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10875
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   10875
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command4 
      Caption         =   "About"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   6840
      TabIndex        =   5
      Top             =   4000
      Width           =   3500
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   6840
      TabIndex        =   3
      Top             =   5400
      Width           =   3500
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Add / Remove Questions"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   6840
      TabIndex        =   2
      Top             =   2600
      Width           =   3500
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00800000&
      Caption         =   "PLAY NOW!"
      DisabledPicture =   "frmInterface.frx":0000
      DownPicture     =   "frmInterface.frx":0639
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   6840
      MaskColor       =   &H00800000&
      Picture         =   "frmInterface.frx":0C72
      TabIndex        =   1
      Top             =   1200
      Width           =   3500
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "v1.3"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   9840
      TabIndex        =   4
      Top             =   6360
      Width           =   975
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00800000&
      Caption         =   "MAIN MENU"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   495
      Left            =   4560
      TabIndex        =   0
      Top             =   360
      Width           =   4575
   End
   Begin VB.Image Image1 
      Height          =   6570
      Left            =   -720
      Picture         =   "frmInterface.frx":12AB
      Top             =   0
      Width           =   7035
   End
End
Attribute VB_Name = "frmInterface"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
frmInterface.Hide
frmPlayNow.Show
frmPlayNow.adoQuest.Recordset.MoveFirst
End Sub

Private Sub Command2_Click()
frmInterface.Hide
frmAddRemove.Show
End Sub

Private Sub Command3_Click()
End
End Sub

Private Sub Command4_Click()
frmAbout.Show
End Sub
