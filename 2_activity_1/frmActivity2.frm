VERSION 5.00
Begin VB.Form frmActivity2 
   BackColor       =   &H00C0C0FF&
   Caption         =   "Paul Mark"
   ClientHeight    =   6360
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4875
   LinkTopic       =   "Form2"
   ScaleHeight     =   6360
   ScaleWidth      =   4875
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   500
      Left            =   1440
      TabIndex        =   1
      Top             =   5640
      Width           =   1500
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   600
      Picture         =   "frmActivity2.frx":0000
      ScaleHeight     =   315
      ScaleWidth      =   1755
      TabIndex        =   12
      Top             =   4680
      Width           =   1815
   End
   Begin VB.ListBox List1 
      Height          =   255
      Left            =   3360
      TabIndex        =   11
      Top             =   3360
      Width           =   855
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   2160
      TabIndex        =   10
      Text            =   "Combo1"
      Top             =   3360
      Width           =   855
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Option1"
      Height          =   255
      Left            =   1080
      TabIndex        =   9
      Top             =   3360
      Width           =   495
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   195
      Left            =   240
      TabIndex        =   8
      Top             =   3360
      Width           =   375
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   3120
      TabIndex        =   7
      Text            =   "Text1"
      Top             =   1440
      Width           =   975
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   500
      Left            =   3120
      TabIndex        =   0
      Top             =   5640
      Width           =   1500
   End
   Begin VB.Image Image1 
      Height          =   285
      Left            =   2880
      Picture         =   "frmActivity2.frx":8267
      Top             =   4800
      Width           =   1050
   End
   Begin VB.Label Label5 
      Caption         =   "Label5"
      Height          =   255
      Left            =   840
      TabIndex        =   6
      Top             =   1440
      Width           =   855
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "ANSWERS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   5
      Top             =   240
      Width           =   4335
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "3. The tools to display images are Picture and Images, Images can be resized while Pictures are not."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   4
      Top             =   3960
      Width           =   4455
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   $"frmActivity2.frx":838A
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   240
      TabIndex        =   3
      Top             =   1800
      Width           =   4455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "1. The tools that are used to display text are Textbox and Label. Labels are just for Output while Textbox are for Input."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   2
      Top             =   840
      Width           =   4335
   End
End
Attribute VB_Name = "frmActivity2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBack_Click()
frmActivity2.Hide
frmActivity1.Show

End Sub

Private Sub cmdClose_Click()

If MsgBox("Are you sure?", vbQuestion + vbYesNo, "Confirm Exit") = vbYes Then End

End Sub
