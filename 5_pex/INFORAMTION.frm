VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00C0E0FF&
   Caption         =   "INFO"
   ClientHeight    =   6390
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7485
   LinkTopic       =   "Form1"
   ScaleHeight     =   6390
   ScaleWidth      =   7485
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbBirthdayM 
      Height          =   315
      ItemData        =   "INFORAMTION.frx":0000
      Left            =   1320
      List            =   "INFORAMTION.frx":0025
      Style           =   2  'Dropdown List
      TabIndex        =   29
      Top             =   2160
      Width           =   735
   End
   Begin VB.ComboBox cmbBirthdayD 
      Height          =   315
      ItemData        =   "INFORAMTION.frx":0082
      Left            =   2160
      List            =   "INFORAMTION.frx":00E3
      Style           =   2  'Dropdown List
      TabIndex        =   28
      Top             =   2160
      Width           =   735
   End
   Begin VB.ComboBox cmbBirthdayY 
      Height          =   315
      ItemData        =   "INFORAMTION.frx":0163
      Left            =   3000
      List            =   "INFORAMTION.frx":01A6
      Style           =   2  'Dropdown List
      TabIndex        =   27
      Top             =   2160
      Width           =   630
   End
   Begin VB.TextBox txtStudentNo 
      Height          =   285
      Left            =   1320
      TabIndex        =   26
      Top             =   1080
      Width           =   2295
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   1320
      TabIndex        =   25
      Top             =   1440
      Width           =   2295
   End
   Begin VB.TextBox txtAddress 
      Height          =   285
      Left            =   5040
      TabIndex        =   24
      Top             =   1080
      Width           =   2295
   End
   Begin VB.TextBox txtBirthPlace 
      Height          =   285
      Left            =   5040
      TabIndex        =   23
      Top             =   1440
      Width           =   2295
   End
   Begin VB.TextBox txtContactNo 
      Height          =   285
      Left            =   5040
      TabIndex        =   22
      Top             =   1800
      Width           =   2295
   End
   Begin VB.TextBox txtNationality 
      Height          =   285
      Left            =   5040
      TabIndex        =   21
      Top             =   2160
      Width           =   2295
   End
   Begin VB.ComboBox cmbSex 
      Height          =   315
      ItemData        =   "INFORAMTION.frx":0228
      Left            =   5040
      List            =   "INFORAMTION.frx":0232
      Style           =   2  'Dropdown List
      TabIndex        =   20
      Top             =   2520
      Width           =   2295
   End
   Begin VB.ComboBox cmbCourse 
      Height          =   315
      ItemData        =   "INFORAMTION.frx":0244
      Left            =   1320
      List            =   "INFORAMTION.frx":0251
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   1800
      Width           =   2295
   End
   Begin VB.ComboBox cmbStatus 
      Height          =   315
      ItemData        =   "INFORAMTION.frx":02B9
      Left            =   1320
      List            =   "INFORAMTION.frx":02C6
      Style           =   2  'Dropdown List
      TabIndex        =   19
      Top             =   2520
      Width           =   2295
   End
   Begin VB.ListBox lstSubjectsChosen 
      Height          =   2205
      ItemData        =   "INFORAMTION.frx":02E5
      Left            =   3720
      List            =   "INFORAMTION.frx":02E7
      TabIndex        =   18
      Top             =   3360
      Width           =   3615
   End
   Begin VB.ListBox lstSubjects 
      Height          =   2205
      ItemData        =   "INFORAMTION.frx":02E9
      Left            =   120
      List            =   "INFORAMTION.frx":02EB
      TabIndex        =   17
      Top             =   3480
      Width           =   3495
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   3120
      TabIndex        =   16
      Top             =   5760
      Width           =   1335
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "Next"
      Height          =   375
      Left            =   4920
      TabIndex        =   15
      Top             =   5760
      Width           =   1335
   End
   Begin VB.CommandButton cmdAddSubject 
      Caption         =   "Add Subject"
      Height          =   375
      Left            =   1320
      TabIndex        =   14
      Top             =   5760
      Width           =   1335
   End
   Begin VB.Label Label13 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "SUBJECT CHOSEN"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3720
      TabIndex        =   13
      Top             =   3000
      Width           =   3615
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "SUBJECTS"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   3000
      Width           =   3495
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   2520
      Width           =   1095
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Sex"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3720
      TabIndex        =   10
      Top             =   2520
      Width           =   1215
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Nationality"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3720
      TabIndex        =   9
      Top             =   2160
      Width           =   1215
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Contact No."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3720
      TabIndex        =   8
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Birth Place"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3720
      TabIndex        =   7
      Top             =   1440
      Width           =   1215
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3720
      TabIndex        =   6
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Birthday"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   2160
      Width           =   1095
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Course"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1800
      Width           =   1095
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Student No."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "PERSONAL INFORMATION"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   7215
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ctr As Integer
Dim flag As Integer

Private Sub cmbCourse_Click()
If cmbCourse = "Bachelor of Computer Science" Then
lstSubjects.Clear
lstSubjectsChosen.Clear

lstSubjects.AddItem ("CSCI01 Introduction to Computing")
lstSubjects.AddItem ("CSCI02 Borland Turbo C")
lstSubjects.AddItem ("CSCI03 Sun JAVA")
lstSubjects.AddItem ("CSCI04 Borland C++")
lstSubjects.AddItem ("CSCI05 Macromedia Flash")
lstSubjects.AddItem ("CSCI06 Assembly Language")
lstSubjects.AddItem ("CSCI07 Microsoft Visual Basic")

End If

If cmbCourse = "Bachelor of Information Technology" Then
lstSubjects.Clear
lstSubjectsChosen.Clear

lstSubjects.AddItem ("COMP01 Knowledge Work and Presentation Skills")
lstSubjects.AddItem ("COMP02 Database")
lstSubjects.AddItem ("COMP03 Introduction to the World Wide Web")

End If

If cmbCourse = "Bachelor of Computer Engineering" Then
lstSubjects.Clear
lstSubjectsChosen.Clear

lstSubjects.AddItem ("MATH01 College Algebra")
lstSubjects.AddItem ("MATH02 College Trigonometry")
lstSubjects.AddItem ("MATH03 Statistics")
lstSubjects.AddItem ("MATH04 Calculus")
lstSubjects.AddItem ("MATH05 Advance Calculus")

End If
End Sub

Private Sub cmdAddSubject_Click()
flag = 0

For x = 0 To lstSubjectsChosen.ListCount - 1
If lstSubjects.List(lstSubjects.ListIndex) = lstSubjectsChosen.List(x) Then flag = 1
Next

If flag = 0 Then
lstSubjectsChosen.AddItem (lstSubjects.Text)
Form2.txtTuitionFee.Text = Val(Form2.txtTuitionFee.Text) + 800
Form2.txtTotalUnits.Text = Val(Form2.txtTotalUnits.Text) + 3
End If


End Sub

Private Sub cmdCancel_Click()
If Not lstSubjectsChosen.ListCount = 0 Then
lstSubjectsChosen.RemoveItem (lstSubjectsChosen.ListIndex)
Form2.txtTuitionFee.Text = Val(Form2.txtTuitionFee.Text) - 800
Form2.txtTotalUnits.Text = Val(Form2.txtTotalUnits.Text) - 3
End If
End Sub

Private Sub cmdNext_Click()
Form1.Hide
Form2.Show
End Sub



